export const params = {
    out: 'build',
    htmlSrc: 'src/pages/index/index.pug',
    levels: ['src/blocks', 'src/pages/index/blocks']
};

export const helpersFile = ['helpers/normalize.css',
                            'helpers/variables.css',
                            'helpers/mixin.css',
                            'helpers/fonts.css'];
