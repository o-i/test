import { params, helpersFile}       from '../params';
import gulp                         from 'gulp';
import pug                          from 'gulp-pug';
import prettify                     from 'gulp-html-prettify';


gulp.task('html', () => {
    return gulp
        .src(params.htmlSrc)
        .pipe(pug({basedir: 'src'}))
        .pipe(prettify({
            brace_style: 'expand',
            indent_size: 1,
            indent_char: '\t',
            indent_inner_html: true,
            preserve_newlines: true
        }))
        .pipe(gulp.dest(params.out));
});
