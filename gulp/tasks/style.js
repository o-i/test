import { params, helpersFile}       from '../params';
import gulp                         from 'gulp';
import fileNames                    from 'html2bl';
import sourcemaps                   from 'gulp-sourcemaps';
import plumber                      from 'gulp-plumber';
import postcss                      from 'gulp-postcss';
import atImport                     from 'postcss-import';
import scssVars                     from 'postcss-simple-vars';
import nested                       from 'postcss-nested';
import mixins                       from 'postcss-mixins';
import autoprefixer                 from 'autoprefixer';
import normalize                    from 'postcss-normalize';
import flexfix                      from 'postcss-flexbugs-fixes';
import cssnano                      from 'gulp-cssnano';
import rename                       from 'gulp-rename';
import gulpIf                       from 'gulp-if'
import mqpacker                     from 'css-mqpacker';
import fs                           from 'fs';


const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'dev';
// При итоговой сборке стилей использовать команду NODE_ENV = production
const getFileNames = fileNames.getFileNames(params);
const css = fs.readFileSync('src/styles/app.css', 'utf8');
const writeStream = fs.createWriteStream('src/styles/app.css', {'flags': 'w'});

function writeImport(fileArr) {
    return fileArr.map(filePath => {
        const fileImport = `@import "${filePath}"; \n`;
        return writeStream.write(fileImport);
    });
};

gulp.task('style', () => {
    getFileNames.then(files => {
        writeImport(helpersFile);
        writeImport(files.css);
        writeStream.end();

        return gulp
            .src('src/styles/app.css')
            .pipe(gulpIf(isDev, sourcemaps.init()))
            .pipe(postcss([
                atImport(),
                scssVars(),
                autoprefixer(),
                normalize(),
                flexfix(),
                mqpacker({
                    sort: false
                })
            ]))
            .pipe(gulpIf(isDev, sourcemaps.write()))
            .pipe(gulpIf(!isDev, cssnano()))
            .pipe(gulpIf(!isDev, rename('style.min.css')))
            .pipe(gulp.dest(params.out))
    })
    .done();
});
